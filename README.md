## Instruction to start App
* export DATABASE_URL=postgres://<username>:<password>@<hostname>:<port>/<databaseName>
* npm install
* node app.js

##  Slides

* https://docs.google.com/presentation/d/1coyg53PC9fXjO4ctwymzfcvV2Nrg8fuP84WdsOu9dqE/edit?usp=sharing

## References

* http://api.openweathermap.org/data/2.5/weather?q=Singapore,sg&appid=<API_KEY>

* https://vast-hamlet-63321.herokuapp.com/

* https://www.facebook.com/Stackup-Clinic-Chatbot-1562463657117269/