module.exports = {
    FB_PAGE_TOKEN: process.env.FB_PAGE_TOKEN || '',
    FB_VERIFY_TOKEN: process.env.FB_VERIFY_TOKEN || '',
    API_AI_CLIENT_ACCESS_TOKEN: process.env.API_AI_CLIENT_ACCESS_TOKEN || '',
    FB_APP_SECRET: process.env.FB_APP_SECRET || '',
    SERVER_URL: process.env.SERVER_URL || "",
    SENGRID_API_KEY: process.env.SENGRID_API_KEY || '',
    EMAIL_FROM: process.env.EMAIL_FROM || '',
    EMAIL_TO: process.env.EMAIL_TO || '',
    WEATHER_API_KEY: process.env.WEATHER_API_KEY || '',
};